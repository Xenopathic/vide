function! vide#lsp#Enable() abort
    if get(g:, 'vide#lsp#autofmt', 1)
        call vide#lsp#AutofmtEnable()
    endif

    let g:LanguageClient_serverCommands = {}
    function! s:DefineCommand(lang, ...) abort
        call vide#lsp#Command(a:lang, a:000)
    endfunction
    command! -nargs=+ VideLspCommand call s:DefineCommand(<f-args>)

    VideLspCommand rust rls

    set formatexpr=LanguageClient#textDocument_rangeFormatting_sync()

    if get(g:, 'vide#lsp#maps', 1)
        call vide#lsp#MapsEnable()
    endif
endfunction

function! vide#lsp#Command(lang, cmd) abort
    let g:LanguageClient_serverCommands[a:lang] = a:cmd
endfunction

function! vide#lsp#MapsEnable() abort
    nnoremap <silent> <F5> :call LanguageClient#contextMenu()<CR>
    nnoremap <silent> K :call LanguageClient#textDocument_hover()<CR>
    nnoremap <silent> gd :call LanguageClient#textDocument_definition()<CR>
    nnoremap <silent> cin :call LanguageClient#textDocument_rename()<CR>
    nnoremap <silent> cic :call LanguageClient#textDocument_rename(
        \ {'newName': Abolish.camelcase(expand('<cword>'))})<CR>
    nnoremap <silent> cis :call LanguageClient#textDocument_rename(
        \ {'newName': Abolish.snakecase(expand('<cword>'))})<CR>
    nnoremap <silent> cil :call LanguageClient#textDocument_rename(
        \ {'newName': Abolish.lowercase(expand('<cword>'))})<CR>
    nnoremap <silent> ciu :call LanguageClient#textDocument_rename(
        \ {'newName': Abolish.uppercase(expand('<cword>'))})<CR>
endfunction

function! vide#lsp#AutofmtEnable() abort
    function! s:AttemptFmt() abort
        if exists('b:vide#lsp#autofmt_buf') && b:vide#lsp#autofmt_buf
            call LanguageClient#textDocument_formatting()
        endif
    endfunction
    command! VideLspAutofmtBuffer let b:vide#lsp#autofmt_buf = 1

    augroup vide_lsp_autofmt
        autocmd BufWritePre * call s:AttemptFmt()
        autocmd InsertLeave * call s:AttemptFmt()
    augroup END
endfunction
